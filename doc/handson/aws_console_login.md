# AWSコンソールログインについて

事前にお渡ししている2つのアカウント情報（Delivery、Runtime）があります。各アカウントでログインしてください。

## 1つのブラウザで完結させる方

1. ブラウザを立ち上げます
   1. 通常モード
   1. シークレットモード
1. [AWSコンソール](https://aws.amazon.com/jp/console/) にて、ログインしてください
   1. Delivery環境
   1. Runtime環境

## 2つのブラウザに分ける方

1. 2つのブラウザを立ち上げます
   1. 例：Chrome
   1. 例：Firefox
1. [AWSコンソール](https://aws.amazon.com/jp/console/) にて、ログインしてください
   1. Delivery環境
   1. Runtime環境

## リージョンの選択

AWSコンソールでは、リージョンごとにリソースを表示させることが出来ます。

[リージョンの選択](https://docs.aws.amazon.com/ja_jp/awsconsolehelpdocs/latest/gsg/getting-started.html#select-region)

ハンズオンでは、 `ap-northeast-1` 東京リージョンを選択しておきましょう。
